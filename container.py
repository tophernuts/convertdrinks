class Container:

    def __init__(self, id, name, shape=-1, svg_id=None):
        self._id = id
        self.name = name
        # Shape Enum: {Rectangle, Triangle, Trapezoid}
        self.shape = shape
        if not svg_id:
            svg_id = "#{}-mask".format(name.replace(" Glass", "").lower())
        self.svg_id = svg_id

    @classmethod
    def from_dict(cls, dict):
	"""
	Shitty constructor-like function used for loading Containers from JSON
	"""
        _id = dict["_id"]
        name = dict["name"]
        if "shape" in dict.keys():
            shape = dict["shape"]
        else:
            shape = None
        if "svg_id" in dict.keys():
            svg_id = dict["svg_id"]
        else:
            svg_id = None
        return Container(_id, name, shape, svg_id)

    def to_dict(self):
	"""
	Used when saving a container to JSON
	"""
        dict ={
            "_id": self._id,
            "name": self.name,
        }

        if self.shape:
            dict["shape"]= self.shape
        if self.svg_id:
            dict["svg_id"]= self.svg_id

        return dict
