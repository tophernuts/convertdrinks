class Ingredient:

    def __init__(self, _id, name, color = "#CABDAD", svg_id=None):
        self._id = _id
        self.name = name
        self.color = color
        self.svg_id = svg_id

    @classmethod
    def from_dict(cls, dict):
	"""
	A constructor when creating an ingredient from JSON
	"""
        _id = dict["_id"]
        name = dict["name"]
        color = dict["color"] if "color" in dict else None
        svg_id = dict["svg_id"] if "svg_id" in dict else None
        return Ingredient(_id, name, color=color, svg_id=svg_id)

    def to_dict(self):
	"""
	Used to create JSON versions of Ingredients
	"""
        dict = {
            "_id": self._id,
            "name": self.name
        }
        if self.color:
            dict["color"] = self.color
        if self.svg_id:
            dict["svg_id"] = self.svg_id
        return dict
