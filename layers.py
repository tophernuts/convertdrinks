class Layer:

    def __init__(self, ingredient_id, type, amount, description):
        self.ingredient_id = ingredient_id
        # Type ENUM: {Liquid, Solid, Action, Garnish, Alcohol}
        self.type = type
        self.amount = amount
        self.description = description

    @classmethod
    def from_dict(cls, dict):
	"""
	Create those Layers from JSON
	"""
        ingredient_id = dict["ingredient_id"]
        type = dict["type"]
        description = dict["description"]
        amount = dict["amount"]
        return Layer(ingredient_id, type, amount, description)

    def to_dict(self):
	"""
	Convert a Layer into a dict, to be stored in JSON
	"""
        return {
            "ingredient_id": self.ingredient_id,
            "type": self.type,
            "amount": self.amount,
            "description": self.description
        }

class LayerFactory:
"""
Magic LayerFactory that is used by drink_list to create a layer
"""

    add_ingredient_types = ["vodka", "spirits-other", "mixers"]
    LAYER_TYPE_LIQUID = 0
    LAYER_TYPE_SOLID = 1
    LAYER_TYPE_ACTION = 2
    LAYER_TYPE_GARNISH = 3
    TOP_UP_PARTS = 1

    @classmethod
    def create(cls, step):
	"""
	Creates a Layer object based on the step that is passed in from the Absolut Drink Step
	"""
        action = step["action"].lower()
        layer = None
        if action == "fill":
            if step["ingredientType"] != "ice":
                pass
                #print "Filling with ingredient {} of type: {}".format(
                 #   step["ingredient"], step["ingredientType"]
                #)
            # TODO: If we figure out mixing cups, change last argument to be step["description"]
            layer = Layer(step["ingredientType"], cls.LAYER_TYPE_SOLID, 1, "Fill with {}".format(step["ingredient"]))
        elif action == "add":
            if step["ingredientType"] not in cls.add_ingredient_types:
                pass
                #print "Adding with ingredient {} of type: {}".format(
                #    step["ingredient"], step["ingredientType"]
                #)

            if step["measurement"].lower() != "part":
                pass
                #print "Measurement is not part, it is: {}".format(step["measurement"])
            ing_id = cls.get_ingredient_id(step["ingredient"])
            layer = Layer(ing_id, cls.LAYER_TYPE_LIQUID, int(step["quantity"]), step["textPlain"])#.replace("Absolut ", ""))
        elif action == "topup":
            # TODO: figure out better way to topup via parts
            layer = Layer(cls.get_ingredient_id(step["ingredient"]), cls.LAYER_TYPE_LIQUID,
                          cls.TOP_UP_PARTS, step["textPlain"])
        elif action == "stir":
            # TODO: This might get weird without different kinds of glasses
            layer = Layer(step["action"], cls.LAYER_TYPE_ACTION, 0, step["textPlain"])
        elif action == "muddle":
            #TODO: Maybe wait until we have different cups for this
            layer = Layer(step["action"], cls.LAYER_TYPE_ACTION, 0, step["textPlain"])
        elif action == "garnish":
            layer = Layer(step["ingredient"].lower(), cls.LAYER_TYPE_GARNISH, 1, step["textPlain"])
        elif action == "blend" or action == "pour" or action == "strain" or action == "shake" or action == "float":
            #TODO: Actually do something
            layer = Layer(step["action"], cls.LAYER_TYPE_ACTION, 0, step["textPlain"])
        else:
            print "Undefined action: {}".format(action)


        return layer

    @classmethod
    def get_ingredient_id(cls, name):
        name = name.lower()
        #name = name.replace("absolut ", "")
        name = name.replace(" ", "_")
        return name
