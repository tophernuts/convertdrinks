This a python project that is used to perform batch edits on your BarTinder Drinks, download more drinks from Absolut, and more.

I've been running it on a ubuntu 14.04 machine with python 2.7.3

To install required libraries:
* pip install requests
* pip install unidecode

This guide assumes your project has both BarTinder and convert drinks in the same root directory
But it will work with any project structures, assuming you provide the correct paths to the functions.

All of the functions are documented, but most should be self-explanatory.

## Usage:
All of this is performed from a python shell in the root of this directory.

Downloading all Absolut drinks
```
#!python
>> from drink_list import DrinkList
>> d = DrinkList()>> d.download_absolut_json(json_path="my_json.JSON", pageSize=9999)
# At this point all of the Absolut drinks will exist in my_json.JSON and can be loaded into drink_list by:
>> d.load_absolut_json("my_json.JSON")
```
### This will take a very long time as we have to download all of the drinks howotomix information from absolut

Saving the drinks you've created/edited to json files
```
#!python
>> d.save_json()
```

Loading BarTinder JSON to perform edits on it
```
#!python
>> from drink_list import DrinkList
>> d = DrinkList()
>> d.load_old_json()
```