import json, requests

from unidecode import unidecode

from new_drink import Drink
from ingredient import Ingredient
from container import Container
from layers import LayerFactory

class DrinkList:
"""
This is the God class used for converting Absolut type drinks to BarTinder type drinks and performing
many other batch functions to all of the drinks.
See README.md for information on how to use.
"""

    offset = 0

    liquids = ["vodka", "spirits-other", "mixers", "gin", "brandy", "rum", "tequila", "others",
               "whisky"]
    solids = ["ice", "fruits", "berries", "spices-herbs"]

    def __init__(self, existing_drinks=0):
	"""
	DrinkList constructor.

	@existing_drinks: An offset that will be used when adding absolut drinks, this offset will be applied
	to their ids.
	Say you have already made 10 drinks by hand, and you want to add in all of the absolut drinks as well.
	Call DrinkList(existing_drinks=10), to ensure that the ids of the Absolut drinks will start at 10, to
	avoid overwriting any of your already existing drinks.
	"""
        self.drinks = {}
        self.ingredients = {}
        self.containers = {}
        self.offset = existing_drinks

    def download_absolut_json(self, params, json_path="abs-25.json", pageSize="3563"):
	"""
	Used to download the list of drinks from the absolut drink database and save it in the json_path.
	
	Use this function to get an updated list of the drinks from the absolut database.
	pageSize is hardcoded because I'm lazy, but it should be set to a high number, in order to download
	all of the drinks into one JSON file, instead of pagination.
	"""
        url = "http://addb.absolutdrinks.com/drinks/"
        # pageSize to specify number of results
        params["apiKey"] = "a63026e32ca348b6a9a98aeab4c69da9"
        params["pageSize"] = pageSize
        print "Downloading JSON..."
        resp = requests.get(url=url, params=params)
        if resp.status_code != 200:
            print "Status of get was: {}".format(resp.status_code)
        data = resp.json()
        with open(json_path, "wb") as json_file:
            json.dump(data, json_file)
        return data

    def load_absolut_json(self, json_path="abs-25.json"):
	"""
	Loads the absolut drinks stored in the JSON file specified by json_path.

	This method was created before some changes were made to the BarTinder drink types, to ensure
	your drinks are in the right format, call:
	convert_solid_ingredients_to_solid_types()
	convert_alcoholic_beverages_to_layerType_alcoholic()
	convert_unicode_to_decimal()
	"""
        print "LayerFactory doesn't recognize difference between solids and liquids when being added."
        print "Fix this before using it for real, or call convert_solid_ingredients_to_solid_types()"
        print "LayerFactory also won't recognize Alcoholic as a layer type, to fix, \
                run convert_alcoholic_beverages_to_layerType_alcoholic"

        with open(json_path, "rb") as json_file:
            print "Loading JSON..."
            data = json.load(json_file)["result"]
        print "Total of {} drinks".format(len(data))
        for drink in data:
            self.add_drink(drink)

    def load_old_json(self, json_path="../BarTinder/app/assets/data/"):
	"""
	This loads the BarTinder JSON, where json_path is assuming your project setup is:
	workpace: - 
		  - BarTinder
		  - convertDrinks
	"""
        with open(json_path + "containers.json", "rb") as container_file:
            containers = json.load(container_file)
        for container in containers:
            self.containers[container["_id"]] = Container.from_dict(container)

        with open(json_path + "drinks.json", "rb") as drink_file:
            drinks = json.load(drink_file)

        for drink in drinks:
            self.drinks[drink["_id"]] = Drink.from_dict(drink)

        with open(json_path + "ingredients.json", "rb") as ingredient_file:
            ingredients = json.load(ingredient_file)

        for ingredient in ingredients:
            self.ingredients[ingredient["_id"]] = Ingredient.from_dict(ingredient)

    def save_json(self, json_path):
	"""
	Saves the json to specified path!
	"""
        containers = [container.to_dict() for container in self.containers.values()]
        with open(json_path + "containers.json", "wb") as container_file:
            json.dump(containers, container_file)

        drinks = [drink.to_dict() for drink in self.drinks.values()]
        with open(json_path + "drinks.json", "wb") as drink_file:
            json.dump(drinks, drink_file)

        ingredients = [ingredient.to_dict() for ingredient in self.ingredients.values()]
        with open(json_path + "ingredients.json", "wb") as ingredient_file:
            json.dump(ingredients, ingredient_file)

    def add_drink(self, old_drink):
	"""
	Helper method which will add a drink to self.drinks if it does not already exist.
	self.drinks is pretty dumb since it is a dictionary where the name of the drink is the key,
	meaning that you can't have two drinks with the same name.
	But this could be easily changed by changin self.drinks to have the Drink._id as its key
	
	This method also doesn't allow any non-alcoholic drinks to be created, by checking if the absolut drinks
	isAlcholic value.
	"""
        if not old_drink["isAlcoholic"]:
            # We only want alcoholic drinks
            return
        if old_drink["id"] in self.drinks.keys():
            print "Drink: {} already exists".format(old_drink["id"])
        else:
            name = old_drink["name"]#.replace("Absolut ", "")
            try:
                print "Adding drink: {}".format(name)
            except UnicodeEncodeError:
                name = unidecode(name)
                print "Converting drink name to: {}".format(name)
                return

            id = str(len(self.drinks) + self.offset)
            container_id = self.get_container(old_drink["servedIn"])
            description = old_drink["descriptionPlain"]#.replace("Absolut ", "")
            layers = self.get_layers(old_drink["id"])
            if not layers:
                return
            tags = self.get_tags(old_drink["tastes"])

            self.drinks[id] = Drink(id, name, container_id, description, layers, tags)

    def get_container(self, container):
	"""
	Creates a container if one doesn't already exist for the container_id.
	If any new containers are created, you'll have to add their shape in the index.html of
	BarTinder
	"""
        container_id = container["id"].replace("-", "_").replace("glass", "container")
        if container_id not in self.containers:
            # TODO: Figure out how to get a shape from container size here
            self.containers[container_id] = Container(container_id, container["text"])
        return container_id

    def get_layers(self, drink_id):
	"""
	Absolut Drinks: https://addb.absolutdrinks.com/docs/drinks
	Absolut HowToMix: https://addb.absolutdrinks.com/docs/howtomix

	Since an actual Absolut Drink object only contains a text description of how to make a drink,
	a list of ingredients and actions needed, but not the actual order, we have to get the howtomix
	information on all of the drinks.
	This is a very shitty way of getting that information (no-caching, no parallelism, if we don't get a
	200 back, we assume that the drink was removed), but it gets the job done. When downloading a bunch of
	drinks from absolut, this will take the longest amount of time.

	The layers for the specified drink are created by passing each "step" to the LayerFactory which adds it
	to the drinks layers array, and then calls create_ingredient, which will create the ingredient if one
	does not already exist for it.
	"""
        url = "http://addb.absolutdrinks.com/drinks/{}/howtomix/".format(drink_id)
        # hardcoded apiKeys woo-hoo
        params = {"apiKey": "a63026e32ca348b6a9a98aeab4c69da9"}
        resp = requests.get(url=url, params=params)
        if resp.status_code != 200:
            print "Status of get was: {}".format(resp.status_code)
            print "I guess drink: {} doesn't exist anymore".format(drink_id)
            return None
            #raise Exception("Shit can't get the howtomix of drink: {}".format(drink_id))
        data = resp.json()
        layers = []
        factory = LayerFactory()
        for step in data["steps"]:
            layer = factory.create(step)
            if layer:
                layers.append(layer)
                self.create_ingredient(step)
        return layers

    def create_ingredient(self, step):
	"""
	Creates the ingredients!

	Tries to convert everything to ascii, since unicode was being annoying for a while, and I was in a rush.
	"""
        if "ingredient" in step.keys():
            ingredient_id = LayerFactory.get_ingredient_id(step["ingredient"])
        else:
            # Ingredient is an action we hope
            # print "Step does not contain an ingredient, assuming action: {}".format(step["action"])
            ingredient_id = step["action"]
        # Check if string name contains unicode characters
        try:
          ingredient_id.decode('ascii')
        except:
          print "Caught an error decoding to ascii"
          ingredient_id = unidecode(ingredient_id)
        if ingredient_id not in self.ingredients:
            self.ingredients[ingredient_id] = Ingredient(ingredient_id, ingredient_id.replace("_", " ").title())

    def get_tags(self, tastes):
	"""
	Returns all of the tags for a drink, currently tags are just whatever the absolut drink has
	in its tastes field.

	You may want to also add the occasion of the drink as a tag.
	Absolut Occasions: https://addb.absolutdrinks.com/docs/occasions
	"""
        #TODO: Should we add occasions here?
        tags = []
        for taste in tastes:
            tags.append(taste["text"])
        return tags

    def add_shot_tag_to_shots(self):
	"""
	Since we wanted more tags, this will add the Shot tag to any drink that is in a
	shot_container
	"""
        for drink in self.drinks.values():
            if drink.container_id == "shot_container" and "Shot" not in drink.tags:
                print "Adding Shot tag to : {}".format(drink.name)
                drink.tags.append("Shot")

    def add_simple_tag(self):
	"""
	If a drink has at most three layers, we'll add the Simple tag to that drink, if it does not already
	contain that tag.
	"""
        for drink in self.drinks.values():
            if len(drink.layers) <= 3:
                if "Simple" not in drink.tags:
                    print "Simple drink: {}".format(drink.name)
                    drink.tags.append("Simple")

    def add_easy_tag(self):
	"""
	Easy drinks are those that contain only liquids in them.
	"""
        tag = "Easy"
        for drink in self.drinks.values():
            easy = False
            for layer in drink.layers:
                if layer.type != 0 and layer.type != 4:
                    easy = False
                    break
                easy = True
            if easy and tag not in drink.tags:
                print "Adding {} to drink {}".format(tag, drink.name)
                drink.tags.append(tag)

    def add_tag_for_ingredient(self, tag, ingredients):
	"""
	Helper method for adding tags to all drinks which contain at least one ingredient
	in @ingredients, where that ingredient is not a garnish
	"""
        for drink in self.drinks.values():
            add_tag = False
            for layer in drink.layers:
                if layer.ingredient_id in ingredients and layer.type != 3:
                    add_tag = True
                    break
            if add_tag and tag not in drink.tags:
                print "Adding tag {} to drink {}".format(tag, drink.name)
                drink.tags.append(tag)

    def capitalize_all_tags(self):
        for drink in self.drinks.values():
            drink.tags = [tag.capitalize() for tag in drink.tags]

    def convert_ids_to_strings(self):
        for drink in self.drinks.values():
            drink._id = str(drink._id)

    def convert_solid_ingredients_to_solid_types(self):
	"""
	Since solids are added the same way that liquids are in absolut drinks, this helper method was created
	in order to convert all non-garnish solids from LayerType.Liquid to LayerType.Solid.

	This list isn't absolute, and might have to be manually updated if absolut adds any more solids
	"""
        solids = ["pumpkin_smash", "grape", "cherry", "orange", "cloves", "cinnamon_cane", "jalapeno", "mandarin",
                  "blackberry", "pear", "passionfruit", "lychee", "cinnamon", "cranberry", "sugar,_superfine",
                  "ground_black_pepper", "apple", "straw", "lime_slice", "black_currant", "sugar", "mint_leaf",
                  "lime", "raspberry", "blood_orange_/_red_grapefruit", "basil", "blueberry", "kiwi",
                  "maraschino_berry", "almond", "melons", "chocolate", "olive", "vanilla", "coffee_bean", "banana",
                  "celery", "ginger", "carrot", "mint", "tomato", "mango", "star_anise", "strawberry",
                  "brown_sugar", "flowers_(edible)", "orange_slice", "cucumber", "melon", "lemon", "grapefruit",
                  "nutmeg", "peach", "petals_(edible)", "pineapple", ]
        for drink in self.drinks.values():
            for layer in drink.layers:
                if layer.type is 0 and layer.ingredient_id in solids:
                    print "Changing {} to a solid".format(layer.ingredient_id)
                    layer.type = 1

    def convert_alcoholic_beverages_to_layerType_alcoholic(self):
	"""
	Absolut Drinks doesn't care whether an ingredient is a liquid or an alcohol, so this helper method will convert
	all alcoholic ingredients from LayerType.Liquid to LayerType.Alcohol.
	We do this, so that we can link only the alcholic beverages to the liquordirect search
	"""
        alcohols = ["tequila", "limoncello", "apricot_brandy", "byrrh", "egg_liqueur", "black_currant_liqueur",
                    "elderflower_liqueur", "peach_brandy", "absolut_vanilia", "coconut_liqueur", "port,_red", "hazelnut_liqueur",
                    "dry_vermouth", "cognac", "violet_liqueur", "bourbon", "blackberry_brandy", "strega", "orient_apple",
                    "fernet_branca", "black_raspberry_liqueur", "ginger_beer", "applejack", "white_wine", "dark_rum_of_jamaican_type",
                    "cherry_liqueur", "prosecco", "galliano", "melon_liqueur", "absolut_berri_acai", "green_mint_liqueur", "chambord",
                    "cointreau", "st._raphael", "vermouth,_sweet", "triple_sec", "mescal", "dubonnet", "peach_schanpps",
                    "aperol", "goldschlager_liqueur", "port,_white", "cherrykran", "scotch_whisky", "orange_liqueur",
                    "banana_liqueur", "dark_rum_(aged)", "amaretto", "maraschino_liqueur", "green_curacao", "cinnamon_schnapps_liqueur",
                    "aged_rum_of_cuban_type", "pisco", "italian_vermouth", "green_chartreuse", "calvados", "creme_de_menthe",
                    "mandarines_liqueur", "champagne", "coffee_liqueur", "rum", "absolut_raspberri", "kummel", "creme_de_menthe,_white",
                    "orange_flavored_brandy", "strawberry_liqueuer", "absolut_apeach", "absolut_100", "vanilla_liqueur", "aged_rum_of_haitian_type",
                    "whisky/honey_liqueur_", "chocolate_liqueur", "absolut_ruby_red", "slivovitz", "sparkling_sake", "aged_rum_from_martinique",
                    "dark_rum", "herbal_liqueur", "sambuca", "blueberry_liqueur", "apple_liqueur", "lillet_blanc", "cider",
                    "honey_liqueur", "light_rum", "plymouth_sloe_gin", "tequila_blanco", "sake", "orange_curacao", "rye_whiskey",
                    "rum_of_haitian_type", "bacardi_151", "dark_cacao_liqueur", "bison_grass_flavored_vodka", "gin", "aquavit",
                    "stout_beer", "cream_liqueur", "red_wine", "passionfruit_liqueur", "sweet_vermouth", "almond_liqueur",
                    "benedictine_dom", "kiwi_liqueur", "whiskey", "reposado_tequila", "madeira", "white_chocolate_liqueur",
                    "vodka", "absolut_mandrin", "pisang_ambon", "raspberry_liqueur", "barack_palinka", "pimms_cup_no_1", "chocolate_and_orange_liqueur",
                    "creme_de_cassis", "malibu_rum", "absinthe", "kahlua", "raspberry_rum", "swedish_punsch", "cachaca",
                    "absolut_peppar", "gin_(old_tom)", "sherry", "overproof_rum", "pastis", "campari", "blackberry_liqueur", "brandy",
                    "blue_curacao", "williams_pear_liqueur", "bourbon,_peach_flavored", "canadian_whisky", "genever", "cherry_brandy",
                    "beer", "white_cacao_liqueur", "pineapple_liqueur", "spiced_rum", "butterscotch_schnapps", "absolut_kurant",
                    "irish_whiskey", "yellow_chartreuse", "white_rum", "absolut_pears", "absolut_wild_tea", "absolut_mango"]
        for drink in self.drinks.values():
            for layer in drink.layers:
                if layer.type is 0 and layer.ingredient_id in alcohols:
                    print "Changing {} to an alcohol".format(layer.ingredient_id)
                    layer.type = 4

    def convert_unicode_to_decimal(self):
	"""
	A bunch of the absolut drinks use the dumb unicode fractions to represent parts in a lot
	of steps. The LayerFactory will treat these unicode values as the value 0, so they have
	to be converted to their decimal values.

	This is done by searching throught the description of each layer of each drink, and adding
	the decimal representation of the unicode number to the layer.amount.
	"""
        for drink in self.drinks.values():
            for layer in drink.layers:
                if self.contains_unicode(layer.description):
                    print "Found a bad boy"
                    layer.amount = self.get_unicode_number(layer.description) + layer.amount

    def contains_unicode(self, str):
	# TODO: Not an absolut list, might be missing 2/3
        return u"\u00be" in str or \
            u"\u00bc" in str or u"\u00bd" in str

    def add_back_absolut_name(self):
	"""
	Previously I had removed the Absolut from all of the ingredients/descriptions/Drink names.
	This however leaves us with a lot of strange sounding ingredients, that would be better suited by just calling
	them by their proper aboslut names
	"""
        absolut_vodkas = ["Peppar", "Kurant", "Mandrin", "Vanilia", "Berri Acai", "Raspberri", "Apeach", "Ruby Red",
                          "Pears", "100", "Wild Tea"]
        absolut_ids = [x.lower().replace(" ", "_") for x in absolut_vodkas]
        print absolut_ids

        for drink in self.drinks.values():
            absolut_vodkas_in_description = [x for x in absolut_vodkas if x in drink.description]
            if absolut_vodkas_in_description:
                drink.description = drink.description.replace(absolut_vodkas_in_description[0], "Absolut {}".format(absolut_vodkas_in_description[0]))
                print "Adding absolut back to Drink description: {}".format(drink.description)
            for layer in drink.layers:
                absolut_vodkas_in_string = [x for x in absolut_vodkas if x in layer.description]
                if absolut_vodkas_in_string:
                    print "Adding absolut back to layer: {}".format(absolut_vodkas_in_string[0])
                    layer.description = layer.description.replace(absolut_vodkas_in_string[0], "Absolut {}".format(absolut_vodkas_in_string[0]))
                absolut_ids_in_string = [x for x in absolut_ids if x in layer.ingredient_id]
                if absolut_ids_in_string:
                    layer.ingredient_id = layer.ingredient_id.replace(absolut_ids_in_string[0], "absolut_{}".format(absolut_ids_in_string[0]))

        for ingredient in self.ingredients.values():
            absolut_ids_in_ingredient = [x for x in absolut_ids if x in ingredient._id]
            if not absolut_ids_in_ingredient:
                continue
            print "Adding absolut back to ingredient {}".format(absolut_ids_in_ingredient[0])
            ingredient._id = ingredient._id.replace(absolut_ids_in_ingredient[0], "absolut_{}".format(absolut_ids_in_ingredient[0]))
            absolut_names_in_ingredients = [x for x in absolut_vodkas if x in ingredient.name]
            ingredient.name = ingredient.name.replace(absolut_names_in_ingredients[0], "Absolut {}".format(absolut_names_in_ingredients[0]))

    def get_unicode_number(self, str):
        if u"\u00be" in str:
            return 0.75
        if u"\u00bc" in str:
            return 0.25
        if u"\u00bd" in str:
            return 0.5
        print "Unknown unicode: {}".format(str)
        return 0

    def give_me_id(self):
      input = ""
      while input != "q":
        input = raw_input("Ingredient_id: ")
        print self.id_of_drink_with_ingredient(input)

    def id_of_drink_with_ingredient(self, ingredient_id):
        for drink in self.drinks.values():
          for layer in drink.layers:
            if ingredient_id == layer.ingredient_id:
              return  drink._id
        return "No id found" 
