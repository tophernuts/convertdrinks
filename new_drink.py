from layers import Layer

class Drink:

    def __init__(self, id, name, container_name, description, layers, tags):
        self._id = id
        self.name = name
        self.container_id = container_name
        self.description = description
        self.layers = layers
        self.tags = tags

    @classmethod
    def from_dict(cls, dict):
	"""
	Constructor from a dictionary to be used when creating a Drink from JSON
	"""
        _id = dict["_id"]
        name = dict["name"]
        container_id = dict["container_id"]
        description = dict["description"]
        layers = []
        for layer in dict["layers"]:
            layers.append(Layer.from_dict(layer))
        tags = dict["tags"]

        return Drink(_id, name, container_id, description, layers, tags)

    def to_dict(self):
	"""
	Turns a Drink into a dictionary, which will then be converted into JSON
	"""
        dict = {
            "_id": self._id,
            "name": self.name,
            "container_id": self.container_id,
            "description": self.description,
            "layers": [],
            "tags": self.tags
        }

        for layer in self.layers:
            dict["layers"].append(layer.to_dict())

        return dict
